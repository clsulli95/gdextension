import os
from conan import ConanFile
from conan.tools.files import copy

class gdextensionRecipe(ConanFile):
    name = "gdextension"
    version = "4.0.1"
    settings = "os", "arch", "build_type"

    def layout(self):
        _os = str(self.settings.os).lower()
        _arch = str(self.settings.arch).lower()
        _build_type = str(self.settings.build_type).lower()
        self.folders.build = os.path.join("gdextension-library", _os, _arch, _build_type)
        self.folders.source = self.folders.build
        self.cpp.source.includedirs = ["include", "gdextension"]
        self.cpp.build.libdirs = ["."] 

    def build_id(self):
        self.info_build.setings.build_type = "Any"

    def package(self):
        # Package all include dirs
        local_include_folder = os.path.join(self.source_folder, self.cpp.source.includedirs[0])
        copy(self, "*.h", local_include_folder, os.path.join(self.package_folder, "include"), keep_path=True)
        copy(self, "*.hpp", local_include_folder, os.path.join(self.package_folder, "include"), keep_path=True)

        local_include_folder = os.path.join(self.source_folder, self.cpp.source.includedirs[1])
        copy(self, "*.h", local_include_folder, os.path.join(self.package_folder, "gdextension"), keep_path=True)
        copy(self, "*.hpp", local_include_folder, os.path.join(self.package_folder, "gdextension"), keep_path=True)
        copy(self, "*.md", local_include_folder, os.path.join(self.package_folder, "gdextension"), keep_path=True)
        copy(self, "*.json", local_include_folder, os.path.join(self.package_folder, "gdextension"), keep_path=True)

        # Package all lib dirs
        for lib_dir in self.cpp.build.libdirs:
            local_lib_folder = os.path.join(self.build_folder, lib_dir)
            copy(self, "*.a", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)
            copy(self, "*.so", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)
            copy(self, "*.lib", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)
            copy(self, "*.dll", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)

    def package_info(self):
        _os = str(self.settings.os).lower()
        _arch = str(self.settings.arch).lower()
        _build_type = str(self.settings.build_type).lower()
        _lib_name = "godot-cpp." + _os + "." + _build_type + "." + _arch
        self.cpp_info.libs = [_lib_name]
        self.cpp_info.includedirs = self.cpp.source.includedirs
