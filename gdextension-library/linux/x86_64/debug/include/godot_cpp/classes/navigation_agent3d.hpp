/**************************************************************************/
/*  navigation_agent3d.hpp                                                */
/**************************************************************************/
/*                         This file is part of:                          */
/*                             GODOT ENGINE                               */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

// THIS FILE IS GENERATED. EDITS WILL BE LOST.

#ifndef GODOT_CPP_NAVIGATION_AGENT3D_HPP
#define GODOT_CPP_NAVIGATION_AGENT3D_HPP

#include <godot_cpp/variant/color.hpp>
#include <godot_cpp/classes/navigation_path_query_parameters3d.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/variant/packed_vector3_array.hpp>
#include <godot_cpp/variant/rid.hpp>
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/variant/vector3.hpp>

#include <godot_cpp/core/class_db.hpp>

#include <type_traits>

namespace godot {

class NavigationPathQueryResult3D;

class NavigationAgent3D : public Node {
	GDEXTENSION_CLASS(NavigationAgent3D, Node)

public:

	RID get_rid() const;
	void set_avoidance_enabled(bool enabled);
	bool get_avoidance_enabled() const;
	void set_path_desired_distance(double desired_distance);
	double get_path_desired_distance() const;
	void set_target_desired_distance(double desired_distance);
	double get_target_desired_distance() const;
	void set_radius(double radius);
	double get_radius() const;
	void set_agent_height_offset(double agent_height_offset);
	double get_agent_height_offset() const;
	void set_ignore_y(bool ignore);
	bool get_ignore_y() const;
	void set_neighbor_distance(double neighbor_distance);
	double get_neighbor_distance() const;
	void set_max_neighbors(int32_t max_neighbors);
	int32_t get_max_neighbors() const;
	void set_time_horizon(double time_horizon);
	double get_time_horizon() const;
	void set_max_speed(double max_speed);
	double get_max_speed() const;
	void set_path_max_distance(double max_speed);
	double get_path_max_distance();
	void set_navigation_layers(uint32_t navigation_layers);
	uint32_t get_navigation_layers() const;
	void set_navigation_layer_value(int32_t layer_number, bool value);
	bool get_navigation_layer_value(int32_t layer_number) const;
	void set_path_metadata_flags(BitField<NavigationPathQueryParameters3D::PathMetadataFlags> flags);
	BitField<NavigationPathQueryParameters3D::PathMetadataFlags> get_path_metadata_flags() const;
	void set_navigation_map(const RID &navigation_map);
	RID get_navigation_map() const;
	void set_target_position(const Vector3 &position);
	Vector3 get_target_position() const;
	Vector3 get_next_path_position();
	double distance_to_target() const;
	void set_velocity(const Vector3 &velocity);
	Ref<NavigationPathQueryResult3D> get_current_navigation_result() const;
	PackedVector3Array get_current_navigation_path() const;
	int32_t get_current_navigation_path_index() const;
	bool is_target_reached() const;
	bool is_target_reachable();
	bool is_navigation_finished();
	Vector3 get_final_position();
	void set_debug_enabled(bool enabled);
	bool get_debug_enabled() const;
	void set_debug_use_custom(bool enabled);
	bool get_debug_use_custom() const;
	void set_debug_path_custom_color(const Color &color);
	Color get_debug_path_custom_color() const;
	void set_debug_path_custom_point_size(double point_size);
	double get_debug_path_custom_point_size() const;
protected:
	template <class T, class B>
	static void register_virtuals() {
		Node::register_virtuals<T, B>();
	}

public:

};

} // namespace godot

#endif // ! GODOT_CPP_NAVIGATION_AGENT3D_HPP